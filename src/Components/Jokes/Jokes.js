import React, { useEffect, useState } from "react";
import "./Jokes.css";

const Jokes = () => {
    const [jokes, setJokes] = useState();
    const [newJokes, setNewJokes] = useState();

    const url = "https://api.chucknorris.io/jokes/random";

    const goNewJokes = () => {
        setNewJokes(jokes);
    };

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url);

            if (response.ok) {
                const jokes = await response.json();
                setJokes(jokes.value);
            }
        };

        fetchData().catch(console.error);
    }, [newJokes]);


    return (
        <div className="Task2">
            <button onClick={goNewJokes}>New jokes</button>
            <p>Joke: {jokes}</p>
        </div>
    );
};

export default Jokes;
